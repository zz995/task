<?php $this->extend('layout/app'); ?>

<?php $this->block('content') ?>

    <?php function sortField($title, $asc, $desc, $current) { ?>
        <?php if ($current == $asc) { ?>
            <a class="sort-field" href="?sort=<?= $desc ?>"><?= $title ?>&nbsp;<i class="fa fa-angle-up"></i></a>
        <?php } elseif ($current == $desc) { ?>
            <a class="sort-field" href="?sort=<?= $asc ?>"><?= $title ?>&nbsp;<i class="fa fa-angle-down"></i></a>
        <?php } else { ?>
            <a class="sort-field" href="?sort=<?= $asc ?>"><?= $title ?>
        <?php } ?>
    <?php } ?>

    <p class="">
        <a href="/task" class="btn btn-success">
            Создать задачу
        </a>
    </p>

    <div class="card">
        <table class="employees table table-bordered table-striped">
            <thead>
                <tr>
                    <th><?= sortField('ID', 'id_asc', 'id_desc', $sort) ?></th>
                    <th><?= sortField('Имя пользователя', 'name_asc', 'name_desc', $sort) ?></th>
                    <th><?= sortField('Почта', 'email_asc', 'email_desc', $sort) ?></th>
                    <th><?= sortField('Статус', 'status_asc', 'status_desc', $sort) ?></th>
                    <th>Задача</th>
                    <th>Операции</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tasks as $task) { ?>
                    <tr>
                        <td><?= htmlspecialchars($task->id, ENT_QUOTES | ENT_SUBSTITUTE) ?></td>
                        <td><?= htmlspecialchars($task->name, ENT_QUOTES | ENT_SUBSTITUTE) ?></td>
                        <td><?= htmlspecialchars($task->email, ENT_QUOTES | ENT_SUBSTITUTE) ?></td>
                        <td>
                            <?php if ($task->isNew()) { ?>
                                <span class="badge badge-primary">Новая</span>
                            <?php } else { ?>
                                <span class="badge badge-success">Выполненная</span>
                            <?php } ?>
                        </td>
                        <td><?= htmlspecialchars($task->task, ENT_QUOTES | ENT_SUBSTITUTE) ?></td>
                        <td>
                            <a href="/task/<?= $task->id ?>" class="btn btn-primary btn-sm">Открыть</a>
                            <?php if (isset($user) && $user->isAdmin()) { ?>
                                <a href="/task/<?= $task->id ?>/edit" class="btn btn-primary btn-sm">Редактировать</a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>

        <div class="container">
            <div class="row justify-content-center">
                <ul class="pagination">
                    <?php for($i = 1; $i <= $count; $i++) { ?>
                        <li class="page-item<?= $i == $page ? ' active' : '' ?>">
                            <a class="page-link" href="?page=<?= $i ?>&sort=<?= htmlspecialchars($sort, ENT_QUOTES | ENT_SUBSTITUTE) ?>"><?= $i ?></a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php $this->endBlock() ?>