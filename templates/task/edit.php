<?php $this->extend('layout/app'); ?>

<?php $this->block('content') ?>
    <div class="card">
        <div class="card-header">Редактирование задачи</div>
        <div class="card-body">
            <form method="POST" action="" enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="status" class="col-md-4 col-form-label text-md-right">Выполененна</label>

                    <div class="col-md-6">
                        <input id="status" type="checkbox" class="<?= isset($errors['status']) ? ' is-invalid' : '' ?>" name="status" value="1" <?= (isset($old['status']) ? $old['status'] : $task->status) == 1 ? 'checked' : '' ?>>

                        <?php if (isset($errors['status'])) { ?>
                            <span class="invalid-feedback">
                                <strong><?= htmlspecialchars($errors['status'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                            </span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="task" class="col-md-4 col-form-label text-md-right">Текст задачи</label>

                    <div class="col-md-6">
                        <textarea id="task" class="form-control<?= isset($errors['task']) ? ' is-invalid' : '' ?>" name="task" required><?= isset($old['task']) ? htmlspecialchars($old['task'], ENT_QUOTES | ENT_SUBSTITUTE) : htmlspecialchars($task->task, ENT_QUOTES | ENT_SUBSTITUTE) ?></textarea>

                        <?php if (isset($errors['task'])) { ?>
                            <span class="invalid-feedback">
                                <strong><?= htmlspecialchars($errors['task'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                            </span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Редактировать
                        </button>
                        <a href="/" class="btn btn-default">
                            Отменить
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $this->endBlock() ?>