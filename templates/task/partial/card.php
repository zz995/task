<div class="card">
    <div class="card-header">Информация о задаче</div>
    <div class="card-body">
        <div class="text-center">
            <img class="task-image" src="<?= htmlspecialchars($task->image, ENT_QUOTES | ENT_SUBSTITUTE) ?>">
        </div>

        <div class="m-3">
            <p>
                <strong>Статус:</strong>
                <?php if ($task->isNew()) { ?>
                    <span class="badge badge-primary">Новая</span>
                <?php } else { ?>
                    <span class="badge badge-success">Выполненная</span>
                <?php } ?>
            </p>
            <p>
                <strong>Имя пользователя:</strong>
                <?= htmlspecialchars($task->name, ENT_QUOTES | ENT_SUBSTITUTE) ?>
            </p>
            <p>
                <strong>Почта:</strong>
                <?= htmlspecialchars($task->email, ENT_QUOTES | ENT_SUBSTITUTE) ?>
            </p>
            <p>
                <strong>Задача:</strong><br>
                <?= nl2br(htmlspecialchars($task->task, ENT_QUOTES | ENT_SUBSTITUTE)) ?>
            </p>
        </div>
    </div>
</div>