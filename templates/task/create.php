<?php $this->extend('layout/app'); ?>

<?php $this->block('content') ?>
    <div class="preview-cancel hidden mb-3">
        <button class="btn btn-info">
            Вернуться к созданию задачи
        </button>
    </div>
    <div class="preview-task"></div>
    <div class="card create-task-card">
        <div class="card-header">Создание задачи</div>
        <div class="card-body">
            <form method="POST" id="createForm" action=""  enctype="multipart/form-data">
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Имя пользователя</label>

                    <div class="col-md-6">
                        <input id="name" type="text" class="form-control<?= isset($errors['name']) ? ' is-invalid' : '' ?>" name="name" value="<?= isset($old['name']) ? htmlspecialchars($old['name'], ENT_QUOTES | ENT_SUBSTITUTE) : '' ?>" required>

                        <?php if (isset($errors['name'])) { ?>
                            <span class="invalid-feedback">
                                <strong><?= htmlspecialchars($errors['name'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                            </span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">E-mail</label>

                    <div class="col-md-6">
                        <input id="email" type="text" class="form-control<?= isset($errors['email']) ? ' is-invalid' : '' ?>" name="email" value="<?= isset($old['email']) ? htmlspecialchars($old['email'], ENT_QUOTES | ENT_SUBSTITUTE) : '' ?>" required>

                        <?php if (isset($errors['email'])) { ?>
                            <span class="invalid-feedback">
                                <strong><?= htmlspecialchars($errors['email'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                            </span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="task" class="col-md-4 col-form-label text-md-right">Текст задачи</label>

                    <div class="col-md-6">
                        <textarea id="task" class="form-control<?= isset($errors['task']) ? ' is-invalid' : '' ?>" name="task" required><?= isset($old['task']) ? htmlspecialchars($old['task'], ENT_QUOTES | ENT_SUBSTITUTE) : '' ?></textarea>

                        <?php if (isset($errors['task'])) { ?>
                            <span class="invalid-feedback">
                                <strong><?= htmlspecialchars($errors['task'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                            </span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="image" class="col-md-4 col-form-label text-md-right">Картинка</label>
                    <div class="col-md-6">
                        <input type="file" id="image" name="image" class="form-control<?= isset($errors['image']) ? ' is-invalid' : '' ?>" required>

                        <?php if (isset($errors['image'])) { ?>
                            <span class="invalid-feedback">
                                <strong><?= htmlspecialchars($errors['image'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                            </span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-6 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Создать
                        </button>
                        <button type="submit" class="btn btn-primary preview">
                            Предварительный просмотр
                        </button>
                        <a href="/" class="btn btn-default">
                            Отменить
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
<?php $this->endBlock() ?>