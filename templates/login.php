<?php $this->extend('layout/app'); ?>

<?php $this->block('content') ?>
<div class="row justify-content-center">
    <div class="col-md-8">
        <div class="card">
            <div class="card-header">Вход</div>

            <div class="card-body">
                <form method="POST">
                    <div class="form-group row">
                        <label for="login" class="col-sm-4 col-form-label text-md-right">Логин</label>

                        <div class="col-md-6">
                            <input id="login" type="text" class="form-control<?= isset($errors['login']) ? ' is-invalid' : '' ?>" name="login" value="<?= isset($old['login']) ? htmlspecialchars($old['login'], ENT_QUOTES | ENT_SUBSTITUTE) : '' ?>" required autofocus>

                            <?php if (isset($errors['login'])) { ?>
                                <span class="invalid-feedback">
                                    <strong><?= htmlspecialchars($errors['login'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                                </span>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">Пароль</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control<?= isset($errors['password']) ? ' is-invalid' : '' ?>" name="password" required>

                            <?php if (isset($errors['password'])) { ?>
                                <span class="invalid-feedback">
                                    <strong><?= htmlspecialchars($errors['login'], ENT_QUOTES | ENT_SUBSTITUTE) ?></strong>
                                </span>
                            <?php } ?>
                        </div>
                    </div>

                    <div class="form-group row mb-0">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="btn btn-primary">
                                Войти
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php $this->endBlock() ?>