<?php

namespace App\View;

use App\Models\UserModel;

class Render
{
    private $templatePath;
    private $extend;
    private $blockNames;
    private $blocks = [];
    private $params = [];
    private $user = null;

    public function __construct(string $templatePath)
    {
        $this->templatePath = $templatePath;
        $this->blockNames = new \SplStack;
    }

    public function render(string $name, array $params = [])
    {
        ob_start();
        $user = $this->user;
        extract($this->params, EXTR_OVERWRITE);
        extract($params, EXTR_OVERWRITE);
        $this->extend = null;
        require $this->templatePath . '/' . $name . '.php';
        $content = ob_get_clean();
        if (!$this->extend) {
            return $content;
        }
        return $this->render($this->extend, $params);
    }

    public function renderBlock($name)
    {
        return @$this->blocks[$name] ?: '';
    }

    public function include($partial, array $params = [])
    {
        extract($params, EXTR_OVERWRITE);
        require $this->templatePath . '/' . $partial . '.php';
    }

    public function block($name)
    {
        $this->blockNames->push($name);
        ob_start();
    }

    public function endBlock()
    {
        $content =  ob_get_clean();
        $name = $this->blockNames->pop();
        $this->blocks[$name] = $content;
    }

    public function extend(string $layout)
    {
        $this->extend = $layout;
    }

    public function addParam(string $key, $param)
    {
        $this->params[$key] = $param;
    }

    public function auth() {
        return isset($this->user);
    }

    public function setUser(UserModel $user)
    {
        $this->user = $user;
    }
}