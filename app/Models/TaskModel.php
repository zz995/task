<?php

namespace App\Models;


class TaskModel extends BaseModel
{
    const STATUS_NEW = 0;
    const STATUS_EXECUTED = 1;

    public $id;
    public $name;
    public $email;
    public $task;
    public $image;
    public $status;

    public function __construct($id, $name, $email, $task, $image, $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->image = $image;
        $this->task = $task;
        $this->status = $status;
    }

    public function isExecuted()
    {
        return $this->status == self::STATUS_EXECUTED;
    }

    public function isNew()
    {
        return $this->status == self::STATUS_NEW;
    }

    static public function count()
    {
        $pdo = static::getPdo();
        $sql = "SELECT count(*) AS c FROM tasks";
        $sth = $pdo->prepare($sql);
        $sth->execute([]);

        return $sth->fetch()['c'];
    }

    static public function list($page, $limit, $sort)
    {
        $pdo = static::getPdo();
        $sql = "SELECT * FROM tasks";

        if (
            in_array($sort, [
                'id_asc', 'id_desc',
                'name_asc', 'name_desc',
                'email_asc', 'email_desc',
                'status_asc', 'status_desc',
            ])
        ) {
            $sql .= ' ORDER BY ' . implode(' ', explode('_', $sort)) . ', id';
        } else {
            $sql .= ' ORDER BY id';
        }

        $offset = ($page - 1) * $limit;
        $sql .= ' LIMIT ' . $offset . ', ' . $limit;

        $sth = $pdo->prepare($sql);
        $sth->execute([]);

        $tasks = [];
        foreach ($sth->fetchAll() as $taskData) {
            $tasks[] = new self(
                $taskData['id'],
                $taskData['name'],
                $taskData['email'],
                $taskData['task'],
                $taskData['image'],
                $taskData['status']
            );
        }

        return $tasks;
    }

    static public function create($name, $email, $task, $image): TaskModel
    {
        $pdo = static::getPdo();
        $sql = "INSERT INTO tasks (`name`, `email`, `task`, `image`) VALUES (?, ?, ?, ?)";


        $sth = $pdo->prepare($sql);
        $sth->execute([$name, $email, $task, $image]);

        return static::findByID($pdo->lastInsertId());
    }

    static public function edit($id, $task, $status)
    {
        $pdo = static::getPdo();
        $sql = "UPDATE tasks SET status = ?, task = ? WHERE id = ?";
        $sth = $pdo->prepare($sql);
        $sth->execute([$status, $task, $id]);
    }

    static public function findByID($id)
    {
        $pdo = static::getPdo();
        $sql = "SELECT * FROM tasks WHERE id = ?";
        $sth = $pdo->prepare($sql);
        $sth->execute([$id]);
        $data = $sth->fetch();

        if (empty($data)) {
            return null;
        }

        return new self($data['id'], $data['name'], $data['email'], $data['task'], $data['image'], $data['status']);
    }
}