<?php

namespace App\Models;

class BaseModel
{
    static public function getPdo(): \PDO
    {
        global $container;
        return $container['pdo'];
    }
}