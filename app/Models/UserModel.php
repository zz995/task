<?php

namespace App\Models;

class UserModel extends BaseModel
{
    const ROLE_ADMIN = 'admin';

    public $id;
    public $login;
    public $password;
    public $role;

    private function __construct($id, $login, $password, $role)
    {
        $this->id = $id;
        $this->login = $login;
        $this->password = $password;
        $this->role = $role;
    }

    public function isAdmin()
    {
        return $this->role == self::ROLE_ADMIN;
    }

    public function passwordVerify($password)
    {
        return password_verify($password, $this->password);
    }

    static public function findByID($id): UserModel
    {
        $pdo = static::getPdo();
        $sql = "SELECT * FROM users WHERE id = ?";
        $sth = $pdo->prepare($sql);
        $sth->execute([$id]);
        $data = $sth->fetch();

        return new self($data['id'], $data['login'], $data['password'], $data['role']);
    }

    static public function findByLogin($login): UserModel
    {
        $pdo = static::getPdo();
        $sql = "SELECT * FROM users WHERE login = ?";
        $sth = $pdo->prepare($sql);
        $sth->execute([$login]);
        $data = $sth->fetch();

        return new self($data['id'], $data['login'], $data['password'], $data['role']);
    }

    static public function login($login, $password)
    {
        $user = self::findByLogin($login);
        return $user->passwordVerify($password);
    }
}