<?php

namespace App\Controllers;

use App\Models\UserModel;
use Illuminate\Database\Eloquent\Model;

class AuthController extends ControllerBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function loginForm()
    {
        return $this->template->render('login');
    }

    public function login()
    {
        $errors = [];

        $login = @$_POST['login'];
        $password = @$_POST['password'];

        if (!UserModel::login($login, $password)) {
            $_SESSION['old'] = $_POST;
            $_SESSION['errors'] = ['login' => 'Неверный логин или пароль'];
            header('Location: /login', true, 302);
            exit();
        }

        $user = UserModel::findByLogin($login);
        $_SESSION['userId'] = $user->id;

        header('Location: /', true, 302);
        exit();
    }

    public function logout()
    {
        session_destroy();
        header('Location: /', true, 302);
        exit();
    }
}