<?php

namespace App\Controllers;

use App\Models\TaskModel;
use App\Models\UserModel;

class TaskController extends ControllerBase
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show($id)
    {
        $task = TaskModel::findByID($id);

        if (empty($task)) {
            return $this->notFound();
        }

        return $this->template->render('task/show', ['task' => $task]);
    }

    public function list()
    {
        $limit = 3;
        $page = intval(@$_GET['page']);
        if ($page < 1) {
            $page = 1;
        }
        $sort = @$_GET['sort'] ?: 'id_desc';

        $count = ceil(TaskModel::count() / $limit);
        $tasks = TaskModel::list($page, $limit, $sort);

        return $this->template->render('task/list', ['tasks' => $tasks, 'count' => $count, 'page' => $page, 'sort' => $sort]);
    }

    public function createForm()
    {
        return $this->template->render('task/create');
    }

    public function editForm($id)
    {
        if (!isset($this->user) || !$this->user->isAdmin()) {
            return $this->accessDenied();
        }

        $task = TaskModel::findByID($id);
        if (empty($task)) {
            return $this->notFound();
        }

        return $this->template->render('task/edit', ['task' => $task]);
    }

    public function edit($id)
    {
        if (!isset($this->user) || !$this->user->isAdmin()) {
            return $this->accessDenied();
        }

        $task = @$_POST['task'] ?: '';
        $task = trim($task);
        $task = $task === '' ? null : $task;
        if ($task === null) {
            $errors['task'] = 'Поле должно быть заполненным';
        }

        $status = intval(@$_POST['status']) == 1 ? 1 : 0;

        if (!empty($errors)) {
            $_SESSION['old'] = $_POST;
            $_SESSION['errors'] = $errors;

            header('Location: /task/' . $id . '/edit', true, 302);
            exit();
        }

        TaskModel::edit($id, $task, $status);
        header('Location: /', true, 302);
        exit();
    }

    public function create()
    {
        $errors = [];

        $name = @$_POST['name'] ?: '';
        $name = trim($name);
        $name = $name === '' ? null : $name;
        if ($name === null) {
            $errors['name'] = 'Поле должно быть заполненным';
        }

        $email = @$_POST['email'] ?: '';
        $email = trim($email);
        $email = $email === '' ? null : $email;
        if ($email === null) {
            $errors['email'] = 'Поле должно быть заполненным';
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {
            $errors['email'] = 'Некорректный e-mail';
        }

        $task = @$_POST['task'] ?: '';
        $task = trim($task);
        $task = $task === '' ? null : $task;
        if ($task === null) {
            $errors['task'] = 'Поле должно быть заполненным';
        }

        $fileName = @$_FILES['image']['tmp_name'];
        if ($fileName === null) {
            $errors['image'] = 'Картинка отсутствует';
        } elseif (! in_array(exif_imagetype($fileName), [IMAGETYPE_PNG, IMAGETYPE_GIF, IMAGETYPE_JPEG])) {
            $errors['image'] = 'Некорректный тип';
        }

        if (!empty($errors)) {
            $_SESSION['old'] = $_POST;
            $_SESSION['errors'] = $errors;

            header('Location: /task', true, 302);
            exit();
        }

        $maxImageWidth = 320;
        $maxImageHeight = 240;
        list($width, $height, $type) = getimagesize($fileName);
        $ratioWidth = ($width / $maxImageWidth);
        $ratioHeight = ($height / $maxImageHeight);
        $ratio = max($ratioHeight, $ratioWidth);

        $newWidth = $width;
        $newHeight = $height;
        if ($ratio > 1) {
            $newWidth /= $ratio;
            $newHeight /= $ratio;
        }

        $src = imagecreatefromstring(file_get_contents($fileName));
        $dst = imagecreatetruecolor($newWidth, $newHeight);
        imagecopyresampled($dst, $src, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
        imagedestroy($src);

        $relPath = '/img/tasks/' . md5(uniqid(rand(), true));
        $publicFolder = __DIR__ . '/../../public/';

        if ($type == IMAGETYPE_PNG) {
            $relPath .= '.png';
            imagepng($dst, $publicFolder . $relPath);
        } elseif ($type == IMAGETYPE_GIF) {
            $relPath .= '.gif';
            imagegif($dst, $publicFolder . $relPath);
        } else {
            $relPath .= '.jpeg';
            imagejpeg($dst, $publicFolder . $relPath);
        }
        imagedestroy($dst);

        TaskModel::create($name, $email, $task, $relPath);

        header('Location: /', true, 302);
        exit();
    }

    public function preview()
    {
        $name = @$_GET['name'];
        $task = @$_GET['task'];
        $email = @$_GET['email'];
        $task = new TaskModel(0, $name, $email, $task, '', 0);
        $html = $this->template->render('task/partial/card', ['task' => $task]);

        return ['html' => $html];
    }
}