<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\View\Render;

class ControllerBase
{
    protected $template;
    protected $user = null;

    public function __construct()
    {
        $this->template = new Render(__DIR__ . '/../../templates');

        if (isset($_SESSION['old'])) {
            $this->template->addParam('old', $_SESSION['old']);
            unset($_SESSION['old']);
        }
        if (isset($_SESSION['errors'])) {
            $this->template->addParam('errors', $_SESSION['errors']);
            unset($_SESSION['errors']);
        }
        if (isset($_SESSION['userId'])) {
            $user = UserModel::findByID($_SESSION['userId']);
            $this->user = $user;
            $this->template->setUser($user);
        }
    }

    public function notFound()
    {
        http_response_code('404');
        return $this->template->render('error/notFound');
    }

    public function accessDenied()
    {
        http_response_code('403');
        return $this->template->render('error/accessDenied');
    }

    public function error()
    {
        http_response_code('500');
        return $this->template->render('error/error');
    }
}