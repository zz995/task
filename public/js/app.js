'use strict';

var showPreview = 0;

$(".preview").on('click', function () {
    showPreview = 1;
});

$('#createForm').on('submit', function(e) {
    if (showPreview) {
        showPreview = 0;

        $.ajax({url: '/task/preview?' + $(this).serialize()}).done(function (jsonResponse) {
            $(".preview-task").empty().html(jsonResponse.html);
            $(".create-task-card").addClass('hidden');
            $(".preview-cancel").removeClass('hidden');
            insertImage();
        });

        return false;
    }
});

$(".preview-cancel").on('click', function (e) {
    $(".preview-task").empty();
    $(".create-task-card").removeClass('hidden');
    $(".preview-cancel").addClass('hidden');
});

function insertImage() {
    var files = $("#image").get(0).files;
    if (files.length == 0) {
        return false;
    }

    var file = files[0];
    var reader = new FileReader();
    reader.readAsBinaryString(file);

    reader.onload = function() {
        $(".task-image").attr('src', 'data:' + file.type + ';base64, ' + btoa(reader.result));
    };
}