<?php

require '../vendor/autoload.php';

$path = @$_SERVER[PATH_INFO];
$method = @$_SERVER[REQUEST_METHOD];

session_start();

global $container;
$container = [];
$container['config'] = require '../config/config.php';
$container['pdo'] = new PDO(
    "mysql:host={$container['config']['database']['host']};dbname={$container['config']['database']['dbname']};charset={$container['config']['database']['charset']}",
    $container['config']['database']['username'],
    $container['config']['database']['password'],
    [
        PDO::ATTR_CASE => PDO::CASE_LOWER,
        PDO::ATTR_PERSISTENT => TRUE,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ]
);

if ($path == '' && $method == 'GET') {
    $handel = ['App\Controllers\TaskController', 'list', []];
}
elseif ($path == '/task' && $method == 'GET') {
    $handel = ['App\Controllers\TaskController', 'createForm', []];
}
elseif ($path == '/task' && $method == 'POST') {
    $handel = ['App\Controllers\TaskController', 'create', []];
}
elseif ($path == '/task/preview' && $method == 'GET') {
    $handel = ['App\Controllers\TaskController', 'preview', []];
}
elseif (preg_match('#^/task/(?P<id>\d+)$#i', $path, $matches) && $method == 'GET') {
    $handel = ['App\Controllers\TaskController', 'show', [$matches['id']]];
}
elseif (preg_match('#^/task/(?P<id>\d+)/edit$#i', $path, $matches) && $method == 'GET') {
    $handel = ['App\Controllers\TaskController', 'editForm', [$matches['id']]];
}
elseif (preg_match('#^/task/(?P<id>\d+)/edit$#i', $path, $matches) && $method == 'POST') {
    $handel = ['App\Controllers\TaskController', 'edit', [$matches['id']]];
}
elseif ($path == '/login' && $method == 'GET') {
    $handel = ['App\Controllers\AuthController', 'loginForm', []];
}
elseif ($path == '/login' && $method == 'POST') {
    $handel = ['App\Controllers\AuthController', 'login', []];
}
elseif ($path == '/logout' && $method == 'POST') {
    $handel = ['App\Controllers\AuthController', 'logout', []];
}
else {
    $handel = ['App\Controllers\ControllerBase', 'notFound', []];
}

try {
    list($controller, $action, $arguments) = $handel;
    $response = (new $controller)->$action(...$arguments);
    if (is_array($response)) {
        header('Content-Type: application/json');
        echo json_encode($response);
    } else {
        echo $response;
    }
} catch (\Throwable $err) {
    http_response_code('500');
    echo '<div style=\'text-align: center\'><h1>500</h1><h2>Ошибка сервера</h2></div>';
}
